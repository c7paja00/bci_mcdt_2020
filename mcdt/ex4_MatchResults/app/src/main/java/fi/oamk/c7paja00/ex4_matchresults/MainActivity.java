package fi.oamk.c7paja00.ex4_matchresults;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends ListActivity {

    // All static variables
    static final String URL = "http://172.20.240.11:7002/";
    // XML node keys
    static final String KEY_RESULT = "match_results"; // parent node
    static final String KEY_MATCH = "match";
    static final String KEY_HOME = "home_team";
    static final String KEY_VISITOR = "visitor_team";
    static final String KEY_HOMEGOALS = "home_goals";
    static final String KEY_VISGOALS = "visitor_goals";
    String xml = "";
    ArrayList<HashMap<String, String>> menuItems = new ArrayList<HashMap<String, String>>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new DownloadXML().execute(URL);

        // selecting single ListView item
        ListView lv = getListView();

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String homeTeam = ((TextView) view.findViewById(R.id.homeTeam)).getText().toString();
                String visitorTeam = ((TextView) view.findViewById(R.id.visitorTeam)).getText().toString();
                String homeGoals = ((TextView) view.findViewById(R.id.homeGoals)).getText().toString();
                String visitorGoals = ((TextView) view.findViewById(R.id.visitorGoals)).getText().toString();

                // Starting new intent
                Intent in = new Intent(getApplicationContext(), SingleMenuItemActivity.class);
                in.putExtra(KEY_HOME, homeTeam);
                in.putExtra(KEY_VISITOR, visitorTeam);
                in.putExtra(KEY_HOMEGOALS, homeGoals);
                in.putExtra(KEY_VISGOALS, visitorGoals);
                startActivity(in);

            }
        });

    }

    private class DownloadXML extends AsyncTask<String, Void, String >{

        @Override
        protected String doInBackground(String... strings) {
            XMLParser parser = new XMLParser();
            xml = parser.getXmlFromUrl(URL); // getting XML

            Log.d("JJP", "XML: "+ xml);
            Document doc = parser.getDomElement(xml); // getting DOM element

            NodeList nl = doc.getElementsByTagName(KEY_MATCH);
            // looping through all item nodes <item>
            for (int i = 0; i < nl.getLength(); i++) {
                // creating new HashMap
                HashMap<String, String> map = new HashMap<String, String>();
                Element e = (Element) nl.item(i);
                // adding each child node to HashMap key => value
                //map.put(KEY_MATCH, parser.getValue(e, KEY_MATCH));
                map.put(KEY_HOME, parser.getValue(e, KEY_HOME));
                map.put(KEY_VISITOR, parser.getValue(e, KEY_VISITOR));
                map.put(KEY_HOMEGOALS, parser.getValue(e, KEY_HOMEGOALS));
                map.put(KEY_VISGOALS, parser.getValue(e, KEY_VISGOALS));

                // adding HashList to ArrayList
                menuItems.add(map);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            // Adding menuItems to ListView
            ListAdapter adapter = new SimpleAdapter(MainActivity.this, menuItems,
                    R.layout.list_item,
                    new String[]{KEY_HOME, KEY_VISITOR, KEY_HOMEGOALS, KEY_VISGOALS}, new int[]{
                    R.id.homeTeam, R.id.visitorTeam, R.id.homeGoals, R.id.visitorGoals});

            setListAdapter(adapter);
        }
    }



}
