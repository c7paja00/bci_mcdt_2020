package fi.oamk.c7paja00.ex4_matchresults;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SingleMenuItemActivity extends AppCompatActivity {

    // XML node keys
    static final String KEY_HOME = "home_team";
    static final String KEY_VISITOR = "visitor_team";
    static final String KEY_HOMEGOALS = "home_goals";
    static final String KEY_VISGOALS = "visitor_goals";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_menu_item);

        // getting intent data
        Intent in = getIntent();

        // Get XML values from previous intent
        String homeTeam = in.getStringExtra(KEY_HOME);
        String visitorTeam = in.getStringExtra(KEY_VISITOR);
        String homeGoals = in.getStringExtra(KEY_HOMEGOALS);
        String visitorGoals = in.getStringExtra(KEY_VISGOALS);

        // Displaying all values on the screen
        TextView lblHomeTeam = (TextView) findViewById(R.id.homeTeam_label);
        TextView lblVisitorTeam = (TextView) findViewById(R.id.visitorTeam_label);
        TextView lblHomeGoals = (TextView) findViewById(R.id.homeGoals_label);
        TextView lblVisitorGoals = (TextView) findViewById(R.id.visitorGoals_label);

        lblHomeTeam.setText(homeTeam);
        lblVisitorTeam.setText(visitorTeam);
        lblHomeGoals.setText(homeGoals);
        lblVisitorGoals.setText(visitorGoals);
    }
}
