package fi.oamk.c7paja00.ex4_jsonparser;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SingleMenuItemActivity extends AppCompatActivity {
    // JSON node keys
    private static final String TAG_NAME = "name";
    private static final String TAG_ADDRESS = "address";
    private static final String TAG_IP = "ip";

    //private static final String TAG_EMAIL = "email";
    //private static final String TAG_PHONE_MOBILE = "mobile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_menu_item);

        // getting intent data
        Intent in = getIntent();

        // Get JSON values from previous intent
        String name = in.getStringExtra(TAG_NAME);
        String address = in.getStringExtra(TAG_ADDRESS);
        String ip = in.getStringExtra(TAG_IP);

        // Displaying all values on the screen
        TextView lblName = findViewById(R.id.name_label);
        TextView lblAddress = findViewById(R.id.address_label);
        TextView lblIp = findViewById(R.id.ip_label);

        lblName.setText(name);
        lblAddress.setText(address);
        lblIp.setText(ip);
    }
}
