var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;
 
var app = express();
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
 
// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;
var collection;
 
const MongoClient = require('mongodb').MongoClient;
// replace the uri string with your connection string.
 
const uri = "mongodb+srv://jjp:####@cluster0-hzkae.azure.mongodb.net/test?retryWrites=true&w=majority"
 
MongoClient.connect(uri, function(err, client) {
  if(err) {
    console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
  }
 
  console.log('Connected...');
  collection = client.db("contacts").collection("contacts");
 
  var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});
 
// CONTACTS API ROUTES BELOW
 
// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}
 
/*  "/contacts"
 *    GET: finds all contacts
 *    POST: creates a new contact
 */
 
app.get("/contacts", function(req, res) {
  collection.find({}).toArray(function(err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get contacts.");
    } else {
      res.status(200).json(docs);  
    }
  });
});
 
app.post("/contacts", function(req, res) {
  var newContact = req.body;
  newContact.createDate = new Date();
 
  if (!(req.body.firstName || req.body.lastName)) {
    handleError(res, "Invalid user input", "Must provide a first or last name.", 400);
  }
 
  collection.insertOne(newContact, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to create new contact.");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});
 
/*  "/contacts/:id"
 *    GET: find contact by id
 *    PUT: update contact by id
 *    DELETE: deletes contact by id
 */
 
app.get("/contacts/:id", function(req, res) {
  collection.findOne({ _id: new ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get contact");
    } else {
      res.status(200).json(doc);  
    }
  });
});
 
app.put("/contacts/:id", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc._id;
 
  collection.updateOne({_id: new ObjectID(req.params.id)}, updateDoc, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update contact");
    } else {
      res.status(204).end();
    }
  });
});
 
app.delete("/contacts/:id", function(req, res) {
  collection.deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete contact");
    } else {
      res.status(204).end();
    }
  });
});
